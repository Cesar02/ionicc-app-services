import { Component, OnInit , NgZone} from '@angular/core';
import { DatosOnlineStore } from '../repository/store/datos-online.store';
import {HttpClient} from '@angular/common/http'
// import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';



import { Platform } from '@ionic/angular';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
  DeliveredNotifications,
} from '@capacitor/push-notifications';
import { FCM } from "@capacitor-community/fcm";
import { filter, Observable, Subject, take, takeUntil, tap } from 'rxjs';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  public currentDatos_Estaticos$ = this.store$.currentData$;
  public currentDatos_Estaticos_Loading$ = this.store$.currentDataLoading$;
  public CurrentPingbiip$ = this.store$.currentGetpingbyip$;
  public CurrentPingbiiploading$ = this.store$.currentGetpingbyipLoading$;
  public unsubscribe$ = new Subject<void>();
  // public currentDatos_Estaticos$ = this.store$.currentDatosEstaticos$;
  // public currentDatos_Estaticos_Loading$ = this.store$.currentnDatosEstaticosLoading$;
  token: string = "";
  index = null;

  constructor( private platform : Platform , private store$ : DatosOnlineStore ) {
  }
  
  ngOnInit(): void {
    this.store$.getDatosEstaticos();
  }

  getPingbyip(data : any , index : any)
  {
    data.show = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.index = index;
    this.store$.Getpingbiip(data.ip);
    // this.CurrentPingbiip$.subscribe( (response)=>{
    //   if(response != null)
    //   {
    //     console.log('by id ping' , response);
    //     const descripcion = response['0'].includes('Con conexión') ? 'con conexión' : 'Sin Conexión';
    //     console.log(descripcion);
    //     data.mensaje = descripcion;
    //     this.store$.setPingbyip(null);
    //   }
    // }).unsubscribe();

    this.CurrentPingbiip$.pipe(
      take(2),
      filter((response) => response != null),
      tap((response: any) => {
        console.log('by id ping' , response , 'index' , index , 'data:',data);
        // const descripcion = response['0'].includes('Con conexión') ? 'con conexión' : 'Sin Conexión';
        // console.log(descripcion);
        data.estado = response.estado;
        data.mensaje = response.mensaje;
        data.show = null;
        this.store$.setPingbyip(null);
      })
     ).subscribe();
   

  }

  getToken1() {
    FCM.getToken()
      .then((result) => {
        this.token = result.token;
        console.log(result.token);
      })
      .catch((err) => console.log(err));
  }


  getToken() {
    if (this.platform.is('capacitor')) {
      PushNotifications.requestPermissions().then((permission) => {
        if (permission.receive == "granted") {

          PushNotifications.addListener('registration', async ({ value }) => {
            this.token = value // Push token for Android
            console.log(this.token)
            const { token: fcm_token } = await FCM.getToken()
             this.token = fcm_token
          
            // Get FCM token instead the APN one returned by Capacitor
            if (this.platform.is('ios')) {
              const { token: fcm_token } = await FCM.getToken()
              this.token = fcm_token
            }
            // Work with FCM_TOKEN
            
            console.log( "el token" , this.token);
          })
        } else {
          // No permission for push granted
          alert('No Permission for Notifications!')
        }
      });
    }
  }




}
