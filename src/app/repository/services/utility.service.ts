import {Injectable, NgZone} from '@angular/core';
import { Platform } from '@ionic/angular';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
  DeliveredNotifications,
} from '@capacitor/push-notifications';
import { FCM } from "@capacitor-community/fcm";


import {LocalNotifications , LocalNotificationSchema  , LocalNotificationsPlugin} from '@capacitor/local-notifications';
import { PluginListenerHandle } from '@capacitor/core';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
import { Observable } from 'rxjs/internal/Observable';
import { Observer } from 'rxjs/internal/types';
import { from } from 'rxjs/internal/observable/from';

// import {BackgroundMode} from 'capacitor-plugin-background-mode'



@Injectable({
  providedIn: 'root'
})

export class UtilityService {

   public topicName = 'super-awesome-topic';
   public cont = 0;
   public loadSound = false;
   public notifications: LocalNotificationSchema[] = [];

  constructor( private platform : Platform , private nativeAudio: NativeAudio  , private zone: NgZone  ) {
    this.initNotificacionesPush();
    this.loadsSounds();
    // BackgroundMode.enable();
    
   }

  /* NOTIFICACIONES PUSH */
  initNotificacionesPush()
  {
    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    if(this.platform.is('capacitor'))
    {
      PushNotifications.requestPermissions().then(result => {
        if (result.receive === 'granted') {
          console.log('permisos concedidos');
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
          // PushNotifications.checkPermissions().catch((re)=>{
          //   console.log(re);
           
          // })
          this.addListeners();
        } else {
          // console.log(result)
          // Show some error
        }
      }
      );

    }
    else{
      console.log('PushNotificaciones.requets.Permission() -> No es Movil')
    }
  }

  addListeners()
  {
    // move to fcm demo
    FCM.subscribeTo({ topic: this.topicName })
          .then((r) => alert(`subscribed to topic ${this.topicName}`))
          .catch((err) => console.log(err));

    PushNotifications.addListener('registration', (token: Token) => {
      // alert('Push registration success, token: ' + token.value);
      alert('Push registration success');
      console.log(token.value);
      FCM.getToken().then((result) => {
        console.log('TOKEM FCM',result.token); // This is token for IOS
    }).catch((err) => console.log('i am Error', err));
    });

    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
      console.log(JSON.stringify(error))
    });

    // PRIMER PLANO - SEGUNDO PLANO
    PushNotifications.addListener(
      'pushNotificationReceived',
      async (notification: PushNotificationSchema) => {
        await LocalNotifications.getDeliveredNotifications().then( (array : any)=>{
            console.log('delivery', JSON.stringify(array.notifications));
            const arrayaux = Array.from<any>(array.notifications);
                /* TEST - NO NOTIFICACIONES REPETIDAS */
                if(!arrayaux.some(data => data.title == notification.data?.titulo))
                {
                  console.log('Entro al if')
                  //PLAY SOUND;
                  this.playSound();
                  ++this.cont 
                  // NOTIFICACION LOCAL
                  LocalNotifications.schedule({
                    notifications:[{
                      title : notification.data?.titulo || 'titulo',
                      body :  notification.data?.descripcion || 'body',
                      id : this.cont || 1,
                      iconColor: '#25D366',
                      // actionTypeId: 'Music',
                      // sound: this.setSound(),
                      // extra: {
                      //   data : notification.data,
                      // }
                    }
                  ]
                  });
              }
              else{
                      console.log('Entro al else');
                      //REPRODUCIMOS SONIDO PERO NO PUSEHAMOS LA MISMA NOTIFI;
                      this.playSound();
                }


        });

       
        /* NORMAL - PUSH */
      //  this.playSound();
      //  console.log('Recivimos Notificación',JSON.stringify(notification));
      //   // alert('Push received: ' + JSON.stringify(notification));
      //   // NOTIFICACION LOCAL
      //   LocalNotifications.schedule({
      //     notifications:[{
      //       title : notification.data?.titulo || 'titulo',
      //       body :  notification.data?.descripcion || 'body',
      //       id : ++this.cont || 1,
      //       iconColor: '#25D366',
      //       // actionTypeId: 'Music',
      //       // sound: this.setSound(),
      //       // extra: {
      //       //   data : notification.data,
      //       // }
      //     }
      //   ]
      //   });

      }
    );


    // CLICK A NOTIFICACIONES PUSH
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        alert('Push action performed: ' + JSON.stringify(notification));
      },
    );

    // CLICK A NOTIFICACIONES LOCALES
    LocalNotifications.addListener('localNotificationActionPerformed', (payload) => {
      console.log('Abrimos notificacion local', JSON.stringify(payload));
      this.eliminarNotifiArray(Number(payload.notification.id));
      --this.cont;
      this.nativeAudio.stop('num1');
      this.loadSound = false;
      // this.nativeAudio.unload('num1');
      // triggers when the notification is clicked.
      // console.log('notification triggered:', payload);
    });
    
  }


 async playSound(){
    //  this.nativeAudio.preloadSimple('num1', 'assets/SD_ALERT_8.mp3').then(
    //   (rest) =>{
    //     console.log('reproduciendoce uwu');
    //     this.nativeAudio.play('num1');
    //     setTimeout(  () =>{ this.nativeAudio.unload('num1') },  4000);
    //   }
    // );
    if(this.loadSound == false)
    {
      console.log('reproduciendoce uwu');
      //Dejamos el sonido infito
      this.nativeAudio.loop('num1');
      this.loadSound = true;

      //Despues de cuatro minutos apgamos el sonido
      setTimeout(() =>{
        this.nativeAudio.stop('num1') 
        this.loadSound = false;
       }
     ,  240000);
    }
  }
  loadsSounds(){
    this.nativeAudio.preloadComplex('num1', 'assets/sound_notifis.mp3',1,1,1);
  }
  eliminarNotifiArray(index : number)
  {
    this.notifications.splice(index-1,1);
  }


  public async handleError(errorServidor: any) {

    let mensajeError: string;

    if (errorServidor.error instanceof ErrorEvent) {

      mensajeError = `Ocurrio un error: ${errorServidor.error.message}`;
      //this.toastrService.error(mensajeError);
    } else {
      // mensajeError = `Backend retorno el codigo de error ${errorServidor.status}: ${errorServidor.message}`;
    }

  }
  public getURLImgLogo(canaldelivery:any) : string {
    let urlimagecanaldelivery = '';
    switch(canaldelivery){
      case 'pedidosya':
        urlimagecanaldelivery ='/cocina/assets/iconos/ico_pedidosya.png';
        break;
      case 'rappi':
        urlimagecanaldelivery ='/cocina/assets/iconos/ico_rappi.png';
        break;
      case 'justo':
        urlimagecanaldelivery ='/cocina/assets/iconos/ico_justo.png';
        break;
      case 'didifood':
        urlimagecanaldelivery ='/cocina/assets/iconos/ico_didifood.png';
        break;
      case 'deliverygo':
        urlimagecanaldelivery ='/cocina/assets/iconos/ico_deliverygo.png';
        break;
      case '':
          urlimagecanaldelivery ='';
          break; 
    }
    return urlimagecanaldelivery;
  }

  public static dominios = ['restaurant.pe', 'quipupos.com', 'restpe.com', 'deliverygo.app'];
  public static restaurantpe = 'restaurant.pe';
  public static deliverygo = 'deliverygo.app';
  public static quipuposcom = 'quipupos.com';
  public static restpecom = 'restpe.com';

public static getDominio(): string {
  const hostName = window.location.hostname;
  // return hostName.substring(hostName.lastIndexOf('.', hostName.lastIndexOf('.') - 1) + 1);
  let hostNameRetorno = hostName.substring(hostName.lastIndexOf(".", hostName.lastIndexOf(".") - 1) + 1);

  if (hostNameRetorno == this.quipuposcom || hostNameRetorno == this.restaurantpe || hostNameRetorno == this.deliverygo) {

  } else {
    hostNameRetorno = this.restaurantpe;
  }

  return hostNameRetorno;
}

/**
 * Entorno de Producción
 */
public static getSubdomain(): string {
  const regexParse = new RegExp('[a-z\-0-9]{2,63}\.[a-z\.]{2,5}$');
  const urlParts : any = regexParse.exec(window.location.hostname);
  let subdominioretorno = window.location.hostname.replace(urlParts[0], '').slice(0, -1);
  if (Array.isArray(urlParts) && urlParts.length > 0) {
    const dominio = urlParts[0];
    if (dominio === this.quipuposcom || dominio === this.restaurantpe || dominio === this.deliverygo) {
    } else {
      subdominioretorno = 'demoperu';
    }
  }
  return subdominioretorno;
}



public static getdominios() {
  return ["restaurant.pe", "quipupos.com", "restpe.com", "deliverygo.app"];
}

public static getrestaurantpe() {
  return "restaurant.pe";
}

public static getquipuposcom() {
  return "quipupos.com";
}

public static getrestpecom() {
  "restpe.com";
}

public static getdeliverygo() {
  return "deliverygo.app";
}

  // public static example(): any {

  // }

  public static esRecpcionista() : boolean{
    const session =  JSON.parse(localStorage['sessiontoken'] || '{}');
    // @ts-ignore
    return session.ruta && session.ruta.includes("recepcionista") ? true : false;
  }

}

