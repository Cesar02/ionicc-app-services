
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { initialDatosStore } from './initial.menu-online.store';
import { DatosApiRepository } from '../api/datos.api.repository';
import { StoreService } from '../services/store.service';
import { HTTP_RESPONSE, PETICION } from '../constans';
// import { MessageService } from 'primeng/api';

import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';


@Injectable({
  providedIn: 'root',
})

export class DatosOnlineStore extends StoreService<any> {

  constructor(
    private datosApiRepository: DatosApiRepository,
    private nativeAudio: NativeAudio
    // private messageService: MessageService,
  ) {
    super(initialDatosStore);
  }

  /**
   * Getters
   */

   //test
   get currentlocalesPrioOConProblemas(){
    return this.state.currentlocalesPrioOConProblemas;
  }


   get currentTxtBusquedaSuscripcion() {
    return this.state.txtBusquedaSuscripcion;
  }

  get currentSuscripciones() {
    return this.state.suscripciones;
  }

  get currentSuscripcion() {
    return this.state.currentSuscripcion;
  }

  get currentInformacionGeneralSuscripcion() {
    return this.state.currentInformacionGeneralSuscripcion;
  }

  get currentInformacionLocalesSuscripcion() {
    return this.state.currentInformacionLocalesSuscripcion;
  }

  //test
  public currentlocalesPrioOConProblemas$ : Observable<any[]> = this.select((state) =>{
    return state.currentlocalesPrioOConProblemas;
  })

  public loadingLocalesPrioritariosOConProblemas$ : Observable<any> = this.select((state) =>{
    return state.loadingLocalesPrioritariosOConProblemas == PETICION.EN_PROCESO;
  })



  public suscripciones$: Observable<any[]> = this.select((state) => {
    return state.suscripciones;
  });

  public loadingBusquedaSuscripcion$: Observable<boolean> = this.select((state) => {
    return state.loadingBusquedaSuscripcion == PETICION.EN_PROCESO;
  });

  public currentInformacionGeneralSuscripcion$: Observable<any> = this.select((state) => {
    return state.currentInformacionGeneralSuscripcion;
  });

  public loadingInformacionGeneralSuscripcion$: Observable<boolean> = this.select((state) => {
    return state.loadingInformacionGeneralSuscripcion == PETICION.EN_PROCESO;
  });

  public currentInformacionLocalesSuscripcion$: Observable<any[]> = this.select((state) => {
    return state.currentInformacionLocalesSuscripcion;
  });

  public loadingInformacionLocalesSuscripcion$: Observable<boolean> = this.select((state) => {
    return state.loadingInformacionLocalesSuscripcion == PETICION.EN_PROCESO;
  });

  public detallecapacitacioneslist$: Observable<any[]> = this.select((state) => {
    return state.detallecapacitaciones;
  })

  public conctatoslist$: Observable<any[]> = this.select((state) =>{
    return state.conctatolist;
  })

  public currentSuscripcion$: Observable<any> = this.select((state) => {
    return state.currentSuscripcion;
  });

  public getBusquedaSuscripcion(dominio:string) {
    this.setTxtBusquedaSuscripcion(dominio);
    this.setState({loadingBusquedaSuscripcion: PETICION.EN_PROCESO});
    this.datosApiRepository.getBusquedaSuscripcion(dominio).subscribe(async (response:any) => {
      this.setState({loadingBusquedaSuscripcion: PETICION.FINALIZADA});
      if (response.tipo == HTTP_RESPONSE.SUCCESS) {
        this.setSuscripciones(response.data);
      } else {
        //this.toastrService.error(response.mensajes);
      }
    });
  }

  public getInformacionGeneralBySuscripcion(dominio:string) {
    this.setState({loadingInformacionGeneralSuscripcion: PETICION.EN_PROCESO});
    this.datosApiRepository.getInformacionGeneralBySuscripcion(dominio).subscribe(async (response:any) => {
      this.setState({loadingInformacionGeneralSuscripcion: PETICION.FINALIZADA});
      if (response.tipo == HTTP_RESPONSE.SUCCESS) {
        this.setInformacionGeneralBySuscripcion(response.data);
      } else {
        //this.toastrService.error(response.mensajes);
      }
    });
  }

  public getInformacionLocalesBySuscripcion(dominio:string,params : any = null) {
    this.setState({loadingInformacionLocalesSuscripcion: PETICION.EN_PROCESO});
    this.datosApiRepository.getInformacionLocalesBySuscripcion(dominio,params).subscribe(async (response:any) => {
      this.setState({loadingInformacionLocalesSuscripcion: PETICION.FINALIZADA});
      if (response.tipo == HTTP_RESPONSE.SUCCESS) {
        this.setInformacionLocalesBySuscripcion(response.data);
      } else {
        //this.toastrService.error(response.mensajes);
      }
    });
  }

  //tes
  public getlocalesPrioOConProblemas(paginas:number , registros: number) {
    //this.setState({loadingLocalesPrioritariosOConProblemas: PETICION.EN_PROCESO});
    this.setState({loadingBusquedaSuscripcion: PETICION.EN_PROCESO});

    this.datosApiRepository.getLocalesPrioritariosOConProblemas(paginas , registros).subscribe(async (response:any) => {
     // this.setState({loadingLocalesPrioritariosOConProblemas: PETICION.FINALIZADA});
     this.setState({loadingBusquedaSuscripcion: PETICION.FINALIZADA});

      if (response.tipo == HTTP_RESPONSE.SUCCESS) {
       // this.setlocalesPrioOConProblemas(response.data);
       this.setSuscripciones(response.data);
      } else {
      //   //this.toastrService.error(response.mensajes);
      }
    });
  }

  public getlistDetalleCapacitaciones(dominio:string) {
    this.setState({loadingInformacionLocalesSuscripcion: PETICION.EN_PROCESO});
   this.datosApiRepository.getListReunion(dominio).subscribe( (response:any) => {
     this.setState({loadingInformacionLocalesSuscripcion: PETICION.FINALIZADA});
     if (response.tipo == HTTP_RESPONSE.SUCCESS) {
       this.detallecapacitacioneslist(response.data);
     } else {
       //this.toastrService.error(response.mensajes);
     }
   })
  }


  public getListConctato(dominio: string){
    this.setState({loadingInformacionLocalesSuscripcion: PETICION.EN_PROCESO});
    this.datosApiRepository.getlistConctatos(dominio).subscribe((response:any)=>{
      this.setState({loadingInformacionLocalesSuscripcion: PETICION.FINALIZADA});
      if (response.tipo == HTTP_RESPONSE.SUCCESS) {
        this.conctatolist(response.data);
      } else {
        //this.toastrService.error(response.mensajes);
      }
    })
  }

  public setSuscripciones(suscripciones : any[]){
    this.setState({suscripciones});
  }

  public setInformacionGeneralBySuscripcion(currentInformacionGeneralSuscripcion : any){
    this.setState({currentInformacionGeneralSuscripcion});
  }


  public setInformacionLocalesBySuscripcion(currentInformacionLocalesSuscripcion : any[]){
    this.setState({currentInformacionLocalesSuscripcion});
  }

  public setCurrentSuscripcion(currentSuscripcion : any){
    this.setState({currentSuscripcion});
  }

  public setTxtBusquedaSuscripcion(txtBusquedaSuscripcion : string){
    this.setState({txtBusquedaSuscripcion});
  }

  public detallecapacitacioneslist(detallecapacitaciones:any[]){
    this.setState({detallecapacitaciones});
  }

public conctatolist(conctatolist:any[]){
    this.setState({conctatolist});
}


/* PEDIDOS - TODO  */

// GET
get currentListPedidos() {
  return this.state.currentListPedidos;
}

get currentCheckPedidoCamino()
{
  return this.state.checkPedidoCamino;
}




//SELECT

public currentCheckPedidoCamino$ : Observable<boolean> = this.select((state) =>{
  return state.checkPedidoCamino;
})

public loadingcurrentCheckPedidoCamino$ : Observable<boolean> = this.select((state) =>{
  return state.loadingCheckPedidoCamino == PETICION.EN_PROCESO;
})


public currentListPedidos$ : Observable<any[]> = this.select((state) =>{
  return state.currentListPedidos;
})

public loadingCurrentListPedidos$ : Observable<boolean> = this.select((state) =>{
  return state.loadingCurrentListPedidos == PETICION.EN_PROCESO;
})

// SET
public setPedidos(currentListPedidos : any[]){
  this.setState({currentListPedidos});
}

public setcheckPedido(checkPedidoCamino : any) {
  this.setState({checkPedidoCamino});
}


//SERVICE

public checkPedidoCamino(id : any , dominio : any)
{
  this.setState({loadingCheckPedidoCamino: PETICION.EN_PROCESO});
  this.datosApiRepository.postCheckPedidoCamino(id , dominio).subscribe((response:any)=>{
    this.setState({loadingCheckPedidoCamino: PETICION.FINALIZADA});
    if (response.tipo == HTTP_RESPONSE.SUCCESS) {
      this.setcheckPedido(response.data);
      this.checkPedidoCaminoSiNotificar(response.data.delivery.delivery_id , response.data.delivery.delivery_estado , dominio);
    } else {
      //this.toastrService.error(response.mensajes);
    }
  })
}

public checkPedidoCaminoSiNotificar(id : any , estado : any , dominio : any)
{
  // this.setState({loadingCheckPedidoCamino: PETICION.EN_PROCESO});
  this.datosApiRepository.postCheckPedidoCaminoNotificar(id , estado , dominio).subscribe((response:any)=>{
    // this.setState({loadingCheckPedidoCamino: PETICION.FINALIZADA});
    if (response.tipo == HTTP_RESPONSE.SUCCESS) {
      this.getListPedidos();
      console.log('perdido despachado y avisado')
    } else {
      //this.toastrService.error(response.mensajes);
    }
  })
}




public getListPedidos(){
  this.setState({loadingCurrentListPedidos: PETICION.EN_PROCESO});
  this.datosApiRepository.getPedidos().subscribe((response:any)=>{
    this.setState({loadingCurrentListPedidos: PETICION.FINALIZADA});
    if (response.tipo == HTTP_RESPONSE.SUCCESS) {
      this.setPedidos(response.data);
    } else {
      //this.toastrService.error(response.mensajes);
    }
  })
}


/* LOGIN - ACCOUNT  */

// GET
get currentDatosLogin() {
  return this.state.datosLogin;
}

//SELECT
public currentDatosLogin$ : Observable<any[]> = this.select((state) =>{
  return state.datosLogin;
})

public loadingCurrentDatosLogin$ : Observable<any[]> = this.select((state) =>{
  return state.loadingLogin;
})

// SET
public setDatosLogin(datosLogin : any){
  this.setState({datosLogin});
}


//SERVICE
public login(params : any){
  this.setState({loadingLogin: PETICION.EN_PROCESO});
  this.datosApiRepository.postLogin(params).subscribe((response:any)=>{
    this.setState({loadingLogin: PETICION.FINALIZADA});
    if (response.tipo == HTTP_RESPONSE.SUCCESS) {
      // this.messageService.add({severity:'success', summary: 'Correcto', detail: 'Logueado correctamente'});
      this.setDatosLogin(response.data);
      const data = {
        token : response.data.token,
        ruta: params?.ruta
      }
      this.requestsaveDataSession(data);
      // this.messageService.clear();

    } else {
      // this.messageService.add({severity:'error', summary:'Error', detail:response.mensajes});
    }
  })
}

  /* LOCAL STORE METODOS  - LOGIN*/
  public requestHasSession()  {
    const session =  JSON.parse(localStorage['sessiontoken'] || '{}');
    // @ts-ignore
    return session.token && session.token != '';
  }

  public requestRuta(){
    const session =  JSON.parse(localStorage['sessiontoken'] || '{}');
    // @ts-ignore
    return session.ruta ? session.ruta : '';
  }

  public requestGetAuthToken() {
    const session = JSON.parse(localStorage['sessiontoken'] || '{}');
    return session && session != '' ? session : '';
  }

  public requestsaveDataSession(session: any): void {
    localStorage.setItem('sessiontoken', JSON.stringify(session));
  }

 public requestDeleteDataSession(): void {
    localStorage.removeItem('sessiontoken');
    localStorage.removeItem('sessionruta');
    this.setDatosLogin(null);
  }

 public requestGetDataSession(): Promise<any> {
    const session = JSON.parse(localStorage['sessiontoken'] || '{}');
    return session ? session : null;
  }


   /* LOCAL STORE METODOS  -  RUTAS*/
    //RUTAS
   public  requestGetAuthRutas() {
    const ruta =  JSON.parse(localStorage['sessionruta'] || '{}');
    // console.log ('ruta metodo' , ruta);
    // return ruta && ruta != '' ? ruta : '';
    return ruta ;
    }
    public requestsaveDataSessionRutas(ruta: any): void {
      localStorage.setItem('sessionruta', JSON.stringify(ruta));
    }
  public requestDeleteDataSessionRutas(): void {
      localStorage.removeItem('sessionruta');
    }

//  public requestGetDataSession(): Promise<any> {
//     const session = JSON.parse(localStorage['sessiontoken'] || '{}');
//     return session ? session : null;
//   }




/* SERVICES - PING  */

// GET
get currentData() {
  return this.state.data;
}

//SELECT
public currentData$ :  Observable<any[]> = this.select((state) =>{
  return state.data;
})

public currentDataLoading$ : Observable<boolean> = this.select((state) =>{
  return state.loadingData == PETICION.EN_PROCESO;
})

// SET
public setData(data : any[]){
  this.setState({data});
}



//SERVICE
public getPingService()
{
  this.setState({loadingData : PETICION.EN_PROCESO});
  this.datosApiRepository.pingService().subscribe((response:any)=>{
    this.setState({loadingData: PETICION.FINALIZADA});
    if  (response.tipo == HTTP_RESPONSE.SUCCESS) {
      this.setData(response.data);
      // console.log('entre');
      // this.nativeAudio.preloadSimple('num1', 'assets/SD_ALERT_8.mp3').then(
      //   (rest) =>{
      //     console.log('reproduciendoce uwu');
      //     this.nativeAudio.play('num1');
      //     setTimeout(  () =>{ this.nativeAudio.unload('num1') },  4000);
      //   }
      // );
      
     
    } else {

      // this.nativeAudio.preloadSimple('uniqueId1', 'assets/reggaedrum_09_167a_bytuneloniration.wav');
  // this.nativeAudio.preloadComplex('uniqueId2', 'path/to/file2.mp3', 1, 1, 0).then(onSuccess, onError);
      // this.nativeAudio.play('uniqueId1', () => console.log('uniqueId1 is done playing'));

      // this.nativeAudio.loop('uniqueId2').then(onSuccess, onError);

      // this.nativeAudio.setVolumeForComplexAsset('uniqueId2', 0.6).then(onSuccess,onError);

      // this.nativeAudio.stop('uniqueId1').then(onSuccess,onError);
      //this.toastrService.error(response.mensajes);
    }
  });

}

/* SERVICES - DATOS ESTATICOS  */

// GET
get currentDatosEstaticos() {
  return this.state.data;
}

//SELECT
public currentDatosEstaticos$ : Observable<any[]> = this.select((state) =>{
  return state.datosEstaticos;
})

public currentnDatosEstaticosLoading$ : Observable<boolean> = this.select((state) =>{
  return state.loadingnData == PETICION.EN_PROCESO;
})

// SET
public setDatosEstaticos(datosEstaticos1 : any[]){
  this.setState({datosEstaticos : [...datosEstaticos1]});
}

//SERVICE
public getDatosEstaticos()
{
  this.setState({loadingnData: PETICION.EN_PROCESO});
  this.datosApiRepository.getDatosEstaticos().subscribe((response:any)=>{
    this.setState({loadingnData: PETICION.FINALIZADA});
    if  (response.tipo == HTTP_RESPONSE.SUCCESS) {
         this.setDatosEstaticos(response.data.servidoresList);
    } else {

    }
  });
}

/* SERVICES - GETPINGBYIP  */

// GET
get currentGetpingbyip() {
  return this.state.data;
}

//SELECT
public currentGetpingbyip$ : Observable<any[]> = this.select((state) =>{
  return state.pingbyIp;
})

public currentGetpingbyipLoading$ : Observable<boolean> = this.select((state) =>{
  return state.loadingPingbyip == PETICION.EN_PROCESO;
})

// SET
public setPingbyip(pingbyIp : any){
  this.setState({pingbyIp});
}

//SERVICE
public Getpingbiip(ip : any)
{
  this.setState({loadingPingbyip: PETICION.EN_PROCESO});
  this.datosApiRepository.getpingbyip(ip).subscribe((response:any)=>{
    this.setState({loadingPingbyip: PETICION.FINALIZADA});
    if  (response.tipo == HTTP_RESPONSE.SUCCESS) {
         this.setPingbyip(response.data);
    } else {

    }
  });
}


}
