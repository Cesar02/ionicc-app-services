import { PETICION } from '../constans';
export interface IDatosStore {
  /**
   * Data global module
   */

  /* PEDIDOS */

  currentListPedidos : any[],
  loadingCurrentListPedidos : string,
  loadingCheckPedidoCamino : string,
  checkPedidoCamino : boolean,

  /* LOGIN - ACCOUNT  */
  datosLogin : any,
  loadingLogin : string,

  /* DATOS PING SERVICES */
  data:any,
  loadingData: string,

  /* DATOS ESTATICOS */
  datosEstaticos : any ,
  loadingDatosEstaticos: string,

  pingbyIp: any,
  loadingPingbyip: string,

}


export const initialDatosStore: IDatosStore = {
  /**
   * Loading data
   */

   /* PEDIDOS */
  currentListPedidos: [],
  loadingCurrentListPedidos : PETICION.SIN_INICIALIZAR,
  loadingCheckPedidoCamino : PETICION.SIN_INICIALIZAR,
  checkPedidoCamino: false,


  /* LOGIN - ACCOUNT */
  datosLogin: null,
  loadingLogin: PETICION.SIN_INICIALIZAR,

  /* DATOS PING SERVICES */
  data: [],
  loadingData:PETICION.SIN_INICIALIZAR,

  /* DATOS ESTATICOS */
  datosEstaticos: [],
  loadingDatosEstaticos: PETICION.SIN_INICIALIZAR,

  /* PINGBYIP */
  pingbyIp: null,
  loadingPingbyip: PETICION.SIN_INICIALIZAR,


};
