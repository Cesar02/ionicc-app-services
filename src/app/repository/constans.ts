export const HTTP_RESPONSE = {
  SUCCESS: '1',
  WARNING: '2',
  ERROR: '3',
  INFO: '4',
  HTTP_200_OK: '200',
  PERMISION_ERROR: '401',
  CODE_NOT_DEFINED: '601',
  MALFORMED_JSON: '701',
  ACCESS_DENIED: '403'
};


export enum PETICION {
  SIN_INICIALIZAR = '1',
  EN_PROCESO = '2',
  FINALIZADA = '3'
}

export const ENDPOINT = {
  GET_LOCALES_PRIORITARIOS_O_CON_PROBLEMAS : "http://bi.restaurant.pe/restaurant/bi/rest/fichatecnica/getLocalesPrioritariosOConProblemas",
  GET_BUSQUEDA_SUSCRIPCION : "http://bi.restaurant.pe/restaurant/bi/rest/fichatecnica/getLocalesBusqueda",
  GET_INFORMACION_GENERAL_BYSUSCRIPCION : "http://bi.restaurant.pe/restaurant/bi/rest/fichatecnica/getInformacionGeneralBySuscripcionLink",
  GET_INFORMACION_LOCALES_BYSUSCRIPCION : "http://bi.restaurant.pe/restaurant/bi/rest/fichatecnica/getInformacionLocalesBySuscripcionLink",
  GET_LIST_REUNIONES:"http://localhost/webapp/bi/rest/reunion/listarPorPaginacion",
  GET_LIST_CONCTATOS:"http://localhost/webapp/bi/rest/contactobi/listarPorPaginacion",

   /* PEDIDOS */

  // GET_PEDIDOS : "http://alfa.quipupos.com/restaurant/public/rest/delivery/obtenerDeliverysMultidominioParaDespachador",
  DOMINIO: "" ,
  GET_PEDIDOS : "bk-cocina/public/rest/delivery/obtenerDeliverysMultidominioParaDespachador",
  POST_PEDIDO_EN_CAMINO_SIN_NOTIFICAR_A_INTEGRACIONES : "restaurant/public/rest/delivery/enCaminoSinNotiticarAIntegraciones",
  POST_PEDIDO_NOTIFICAR_A_INTREGRACIONES : "restaurant/public/rest/delivery/notificarAIntegracionesPorDelivery",

  /* LOGIN - ACCOUNT - TEMPORAL */
  // POST_LOGIN : "bk-cocina/public/rest/usuario/login",
  // GET_PEDIDOS : "https://microservices.restaurant.pe/darkkitchen/public/rest/delivery/obtenerDeliverysMultidominioParaDespachador/demoperu.restaurant.pe",
  // POST_PEDIDO_EN_CAMINO_SIN_NOTIFICAR_A_INTEGRACIONES : "http://demoperu.restaurant.pe/restaurant/api/rest/delivery/enCaminoSinNotiticarAIntegraciones",
  // /* LOGIN - ACCOUNT - TEMPORAL */
  POST_LOGIN : "restaurant/m/rest/usuario/login",
  PING_SERVICES: 'services/consultarping',
  DATOS_ESTATICOS: 'services/obtenerDatosEstaticos',
  PING_SERVICES_BY_ID: 'services/consultarpingbyip',
  // PING_SERVICES: 'implementacionbi/obtenerDatosEstaticos',
}
