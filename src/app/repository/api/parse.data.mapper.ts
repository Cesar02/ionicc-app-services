import { HTTP_RESPONSE } from "../constans";

export class ParseDataMapper {
  public static parseResponse(data: any) {
    let dataObj = {tipo: HTTP_RESPONSE.MALFORMED_JSON, mensajes: ['JSON mal formado', data], data: null};
    if (data !== null && typeof data === 'object') {
      if (data.data !== null && typeof data.data === 'object') {
        dataObj = data;
      } else {
        /**
         * Si la respuesta es pura sin tipo ni mensajes seteo data a la respuesta
         */
        if (data.data && data.tipo && data.mensajes) {
          dataObj = data;
        } else if (!data.data && data.tipo && (data.mensajes || data.mensaje)) {
          dataObj = data;
        } else {
          if (!data.tipo && !data.mensajes) {
            dataObj = {tipo: HTTP_RESPONSE.SUCCESS, mensajes: [], data};
            if (data.mensajes && Array.isArray(data.mensajes)) {
              dataObj = {tipo: HTTP_RESPONSE.SUCCESS, mensajes: data.mensajes, data};
            }
            if (data.mensajes && !Array.isArray(data.mensajes)) {
              dataObj = {tipo: HTTP_RESPONSE.SUCCESS, mensajes: [data.mensajes], data};
            }
          } else {
            if (data.tipo === HTTP_RESPONSE.PERMISION_ERROR) {
              dataObj = data;
            }
            if (data.tipo === HTTP_RESPONSE.SUCCESS) {
              if (data.mensajes && Array.isArray(data.mensajes)) {
                dataObj = {tipo: data.tipo, mensajes: data.mensajes, data};
              } else {
                dataObj = {tipo: data.tipo, mensajes: [data.mensajes], data};
              }
            }
            if (data.tipo === HTTP_RESPONSE.WARNING) {
              if (data.mensajes && Array.isArray(data.mensajes)) {
                dataObj = {tipo: data.tipo, mensajes: data.mensajes, data};
                dataObj = {tipo: data.tipo, mensajes: data.mensajes, data};
              } else {
                dataObj = {tipo: data.tipo, mensajes: [data.mensajes], data};
              }
            }
            if (data.tipo === HTTP_RESPONSE.INFO) {
              if (data.mensajes && Array.isArray(data.mensajes)) {
                dataObj = {tipo: data.tipo, mensajes: data.mensajes, data};
              } else {
                dataObj = {tipo: data.tipo, mensajes: [data.mensajes], data};
              }
            }
            if (data.tipo === HTTP_RESPONSE.ERROR) {
              if (data.mensajes && Array.isArray(data.mensajes)) {
                /**
                 * Si la respuesta tiene alguno de los siguientes problemas parseo
                 * la respuesta indicando el origen del problema
                 */

                dataObj = {tipo: data.tipo, mensajes: [''], data};
              } else {
                dataObj = {tipo: data.tipo, mensajes: [data.mensajes], data};
              }
            }
          }
        }
      }
    }
    return dataObj;
  }
}
