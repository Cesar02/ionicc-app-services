import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ParseDataMapper} from './parse.data.mapper';
import {ENDPOINT} from '../constans';
import {UtilityService} from '../services/utility.service';
import {environment} from 'src/environments/environment'
// import {LocalRepositoryMapper} from '../mapper/local-repository.mapper';


@Injectable({
  providedIn: 'root'
})

export class DatosApiRepository {

  // private localRepositoryMapper = new LocalRepositoryMapper();
    public url = `${environment.urlbase}`;
    public apiurl  = `${environment.APIURL}`;

  constructor(
    private http: HttpClient,
    private utilityService: UtilityService,
  ) {
  }

  public getLocalesPrioritariosOConProblemas(pagina : number , registros : number) : Observable<any>{
    return this.http.post(`${ENDPOINT.GET_LOCALES_PRIORITARIOS_O_CON_PROBLEMAS}/${pagina}/${registros}`,null)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }

  public getBusquedaSuscripcion(dominio:string): Observable<any> {
    return this.http.get( `${ENDPOINT.GET_BUSQUEDA_SUSCRIPCION}/${dominio}`)
    .pipe(
      map(ParseDataMapper.parseResponse),
      catchError(responseError => this.utilityService.handleError(responseError))
    );

  }

  public getInformacionGeneralBySuscripcion(dominio: string): Observable<any> {
    return this.http.get(`${ENDPOINT.GET_INFORMACION_GENERAL_BYSUSCRIPCION}/${dominio}`)
      .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
      );
  }

  public getInformacionLocalesBySuscripcion(dominio: string,params: any = null): Observable<any> {
    let periodo = '1';
    if(params && params.filtroperiodo){
      periodo = params.filtroperiodo;
    }
    return this.http.get(`${ENDPOINT.GET_INFORMACION_LOCALES_BYSUSCRIPCION}/${dominio}/${periodo}`)
      .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
      );
  }


  public getListReunion( dominio: string,pagina = 1, cantidadpagina = 10, localbi_id = -1): Observable<any> {
    return this.http.post(`${ENDPOINT.GET_LIST_REUNIONES}/${pagina}/${cantidadpagina}`, {dominio:dominio,localbi_id: localbi_id,listaPersonas: true})
      .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
      );
  }


  public getlistConctatos(dominio: string,pagina = 1, cantidadpagina = 10, localbi_id = "-1"): Observable<any> {
    return this.http.post(`${ENDPOINT.GET_LIST_CONCTATOS}/${pagina}/${cantidadpagina}`, {dominio:dominio,"localbi_id": localbi_id,listaPersonas: true})
      .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
      );
  }

   /* PEDIDOS */
  public getPedidos(): Observable<any> {
    return this.http.get(`${this.url}/${ENDPOINT.GET_PEDIDOS}/${this.apiurl}`)
      .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
      );
  }

  public postCheckPedidoCamino(id : any , dominio : any) : Observable<any>{
    return this.http.post(`https://${dominio}/${ENDPOINT.POST_PEDIDO_EN_CAMINO_SIN_NOTIFICAR_A_INTEGRACIONES}/${id}`,null)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }
  public postCheckPedidoCaminoNotificar(id : any , estado : any , dominio : any) : Observable<any>{
    return this.http.post(`https://${dominio}/${ENDPOINT.POST_PEDIDO_NOTIFICAR_A_INTREGRACIONES}/${id}/${estado}`,null)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }


  /* LOGIN */
  public postLogin(params : any) : Observable<any>{
    return this.http.post(`${this.url}/${ENDPOINT.POST_LOGIN}`,params)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }

  /* SERVICES - PING */
  public pingService() : Observable<any>{
    return this.http.get(`${this.url}/public/rest/${ENDPOINT.PING_SERVICES}`)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }
  /* SERVICES - DATOS ESTATICOS */
  public getDatosEstaticos() : Observable<any>{
    return this.http.get(`${this.url}/public/rest/${ENDPOINT.DATOS_ESTATICOS}`)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }
  /* SERVICES - DATOS ESTATICOS */
  public getpingbyip(ip : any) : Observable<any>{
    return this.http.get(`${this.url}/public/rest/${ENDPOINT.PING_SERVICES_BY_ID}/${ip}`)
    .pipe(
        map(ParseDataMapper.parseResponse),
        catchError(responseError => this.utilityService.handleError(responseError))
    );
  }


}
