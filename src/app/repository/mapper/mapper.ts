export abstract class Mapper<I> {
  abstract mapFrom(param: I): any;

  abstract mapTo(param: any): I;

  abstract mapFromList(param: I[]): any[];

  abstract mapToList(param: any[]): I[];
}
