import { Component } from '@angular/core';
import { UtilityService } from './repository/services/utility.service';
import { DatosOnlineStore } from './repository/store/datos-online.store';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

 constructor(private store$ : DatosOnlineStore , private utily : UtilityService) {
    // this.utily.initNotificacionesPush();
    this.store$.getPingService();
    // setInterval(() => {
    //   this.store$.getPingService();
    //   console.log('1 min segu refresh')
    //  }, 60000);
  }

}
